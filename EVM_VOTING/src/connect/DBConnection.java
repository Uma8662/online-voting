
package connect;

import java.sql.Connection;
import java.sql.DriverManager;
 
public class DBConnection {
 public static Connection createConnection()
 {
 Connection con = null;
 String url = "jdbc:mysql://localhost:3306/uma"; 
 String username = "root";
 String password = "1234"; 
  
 try 
 {
 try 
 {
 Class.forName("com.mysql.cj.jdbc.Driver"); //loading mysql driver 
 } 
 catch (ClassNotFoundException e)
 {
 e.printStackTrace();
 } 
 con = DriverManager.getConnection(url, username, password); 
 System.out.println("Printing connection object "+con);
 } 
 catch (Exception e) 
 {
 e.printStackTrace();
 }
 return con; 
 }
}